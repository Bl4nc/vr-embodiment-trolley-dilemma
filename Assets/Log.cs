using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class Log : MonoBehaviour
{
    public bool logging;
    public string participant;
    public float intervall;
    public GameObject car;
    private string name;
    private float counter;
    private string buffer;
    
    // Start is called before the first frame update
    void Start()
    {
        initLog();

    }

    void initLog ()
    {
        name = "UserLogs\\log" + participant + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss") + ".csv";


        if (!File.Exists(name))
        {
            Debug.Log("created log File: " + name);

            StreamWriter sw = File.CreateText(name);

            using (sw)
            {
                sw.WriteLine("timestamp;x;y;z");
            }

            InvokeRepeating("logData", 0f, intervall);
        }

    }
    void logData()
    {

        float timestamp = Mathf.Round((intervall * counter++) * 1000) / 1000;

        buffer += timestamp + ";" + car.transform.position.x + ";" + car.transform.position.y + ";" + car.transform.position.z + Environment.NewLine;

        if (counter % 10 == 0)
        {
            using (StreamWriter sw = File.AppendText(name))
            {
                sw.Write(buffer);
                buffer = "";
            }

        }
 
       
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
