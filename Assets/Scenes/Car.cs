using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Car : MonoBehaviour
{
    // Start is called before the first frame update
    private Gamepad gamepad;
    private Rigidbody rigid;
    private AudioSource carEngine;
    private AudioSource gravelSound;
    public AudioClip carEngineClip;
    public AudioClip gravelSoundClip;
    private bool wheelConnected;
    private bool isColliding = false;
	private bool rotationAllowed = false;
  
    public GameObject car;
    public float target_speed;
    private float speed;

    public bool start = false;
    public float wheelAlternative;
    public float rotValue;



    void Start()
    {
        carEngine = gameObject.AddComponent<AudioSource>();
        carEngine.volume = 0.2f;
        gravelSound = gameObject.AddComponent<AudioSource>();
        gravelSound.volume = 1.0f;
        carEngine.clip = carEngineClip;
        gravelSound.clip = gravelSoundClip;

        gamepad = Gamepad.current;
        rigid = car.GetComponent<Rigidbody>();

        if (gamepad == null)
        {
            wheelConnected = false;
            Debug.Log("No device found");
        }
        else
        {
            wheelConnected = true;
        }

 

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (start && !carEngine.isPlaying)
        {
            carEngine.Play();
            Invoke("InitSpeed", 4.0f);
        }
       
        Turn();
        
        if (!isColliding) {
            Move();
        }
       
       // Debug.Log(rigid.velocity);
        
    }

    void InitSpeed()
    {
        rotationAllowed = true;
        speed = target_speed;
        if (start)
        {
            gravelSound.Play();
        }
    }
    void Turn()
    {
        if (rotationAllowed)
        {

            if (wheelConnected)
            {
                rotValue = gamepad.leftStick.ReadValue().x;
            }
            else
            {
                rotValue = wheelAlternative;
            }

            Vector3 newRotation = new Vector3(car.transform.eulerAngles.x, (rotValue / 2) * 180, car.transform.eulerAngles.z);
            transform.eulerAngles = newRotation;

            //Debug.Log(gamepad.leftStick.ReadValue());
        }

    }

    void Move()

    {
        if (rigid.velocity.z < speed)
        {
            rigid.AddRelativeForce(Vector3.forward * (speed - rigid.velocity.z));
        }
        
        Debug.Log(rigid.velocity.z);
       
    }


    void OnCollisionEnter(Collision col)
    {
     
        string colName = col.gameObject.name;

 

        if (colName == "Landscape.001")
        {
            isColliding = true;
            rigid.constraints = RigidbodyConstraints.FreezePositionY;
            rigid.transform.position = new Vector3(rigid.transform.position.x + 8, rigid.transform.position.y, rigid.transform.position.z);
        }
        if (colName == "Landscape.003")
        {
            isColliding = true;
            rigid.constraints = RigidbodyConstraints.FreezePositionY;
            rigid.transform.position = new Vector3(rigid.transform.position.x - 8, rigid.transform.position.y, rigid.transform.position.z);
        }
        if (colName.Contains("RoadCone"))
        {
            isColliding = true;
            rigid.constraints = RigidbodyConstraints.FreezePositionY;
            rigid.transform.position = new Vector3(rigid.transform.position.x, rigid.transform.position.y, rigid.transform.position.z -1);
            this.GetComponent<AudioSource>().Play();
        }
        if (colName.Contains("RoadCone"))
        {
            isColliding = true;
            rigid.constraints = RigidbodyConstraints.FreezePositionY;
            rigid.transform.position = new Vector3(rigid.transform.position.x, rigid.transform.position.y, rigid.transform.position.z - 1);
            this.GetComponent<AudioSource>().Play();
        }
        if (colName == "neutral_sacrifice1")
        {
            // End / restarts the simulation if the sacrifice gets hit or the suicide route is chosen
            isColliding = true;
            start = false;
            carEngine.Stop();
            gravelSound.Stop();
        }
		if (colName == "Finish")
		{
			start = false;
			target_speed = 0;
			carEngine.Stop();
            gravelSound.Stop();
		}
    }

    private void OnCollisionExit(Collision col)
    {
         
        if (col.gameObject.name != "plane")
        {
            rigid.constraints = RigidbodyConstraints.None;
            isColliding = false;
        }
               
       
    }
}

