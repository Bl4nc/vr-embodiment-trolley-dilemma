using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class CarPivot : MonoBehaviour
{


    public GameObject carpivot;
    private float wheelAlternative;

    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        Turn();
    }

    void Turn()
    {

        Vector3 newRotation;

        wheelAlternative = carpivot.transform.GetChild(0).GetComponent<Car>().rotValue;
        float rotValue = -wheelAlternative;

        newRotation = new Vector3(carpivot.transform.eulerAngles.x, (rotValue / 2) * 180, carpivot.transform.eulerAngles.z);
        transform.eulerAngles = newRotation;
    }

}

