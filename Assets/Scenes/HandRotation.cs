using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class HandRotation : MonoBehaviour
{


    public GameObject handsPivot;
    private float wheelAlternative;

    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        Turn();
    }

    void Turn()
    {

        Vector3 newRotation;

        wheelAlternative = handsPivot.transform.parent.parent.parent.parent.parent.parent.parent.GetComponent<Car>().rotValue;
        float rotValue = -wheelAlternative;

        newRotation = new Vector3(handsPivot.transform.eulerAngles.x, handsPivot.transform.eulerAngles.y, (rotValue / 2) * 180);
        transform.eulerAngles = newRotation;
    }

}

