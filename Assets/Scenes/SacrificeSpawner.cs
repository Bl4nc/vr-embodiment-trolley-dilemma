using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SacrificeSpawner : MonoBehaviour
{
    private GameObject sacrifice;

    // Start is called before the first frame update
    void Start()
    {
        sacrifice = GameObject.Find("neutral_sacrifice1");
        if(!sacrifice)
        {
            Debug.Log("Couldn't find Sacrifice");
        }
        sacrifice.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if(col.name == "car")
        {
            sacrifice.SetActive(true);
        }
    }
}
