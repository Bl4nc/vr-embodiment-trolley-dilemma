using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SteeringWheels : MonoBehaviour
{
   

    public GameObject wheel;
    private float wheelAlternative;

    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        Turn();
    }

    void Turn()
    {
       
            Vector3 newRotation;

            wheelAlternative = wheel.transform.parent.GetComponent<Car>().rotValue;
            float rotValue = -wheelAlternative;
           
            newRotation = new Vector3(wheel.transform.eulerAngles.x, wheel.transform.eulerAngles.y, (rotValue / 2) * 180);
            transform.eulerAngles = newRotation;
        }

    }

