using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class log : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Vector3 pos = this.transform.parent.gameObject.transform.InverseTransformPoint(this.GetComponent<Renderer>().bounds.center);

        Debug.Log("x" + pos.x);
        Debug.Log("y" + pos.y);
        Debug.Log("z" + pos.z);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
