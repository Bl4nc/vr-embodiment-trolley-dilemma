VR embodiment with the topic trolley dilemma for the course User psychology for Virtual Reality at the University of Applied Sciences in Coburg. In this study the testsubject drives a car and will be faced with moral dilemma. The subject has to choose between sacrificing himself or run over the pedestrian.

A video of the simulation can be found in the VideoAndResult folder.
